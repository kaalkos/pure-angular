app.controller('policiesController',['$scope','$location','$rootScope',
    function($scope,$location,$rootScope,$localStorage,Restangular,policiesAPI){

    //INITIALIZE JSON OBJECT FOR EACH POLICY
      var CommonJsonToSend = {};
      // first cancellation policy added by default
      $scope.cancellations = [{
      "id": 1,
      "name":"Non refundable",
      "created": new Date(),
      "updated": new Date(),
      "rule":[{"daysBefore":"999","fee":"100","type":"%"}]
      }];
      $scope.children = {};
      $scope.depositPolicies = [];
    
    // local storage
    // if($localStorage.cancellations){
    //     $scope.cancellations = $localStorage.cancellations;
    //   }
    // if($localStorage.children){
    //     $scope.children = $localStorage.children;
    //   }
    // if($localStorage.depositPolicies){
    //     $scope.depositPolicies = $localStorage.depositPolicies;
    //   }

    //
    // DEPOSITS
    //
    $scope.deposits = false;
    $scope.depositAdditionalPayment = false;
    $scope.depositAdditionalPayment1 = false;

    $scope.deposit = {};
    $scope.deposit.amount = 30;
    $scope.deposit.daysBefore = 0;
    $scope.deposit.toDelete;

    var ids = 
    {
        deposit: 0,
        cancellation: 1,
        children: 0
    };

    $scope.toggleDeposits = function(range){
        if(range == "all")
        {
            $scope.deposits = !$scope.deposits;
            if($scope.depositAdditionalPayment1){
                $scope.depositAdditionalPayment1 = false;
            }

        }

        else if(range == "additional"){$scope.depositAdditionalPayment = !$scope.depositAdditionalPayment;}

        else if(range == "additional1"){$scope.depositAdditionalPayment1 = !$scope.depositAdditionalPayment1;}
    };

    $scope.editDeposit = function(depositEdited){
        $scope.editDep =  depositEdited + 1;
        $scope.deposit.policyName = $scope.depositPolicies[depositEdited].name;
        $scope.deposit.amount = $scope.depositPolicies[depositEdited].amount;
       
        //first additional payment
        if($scope.depositPolicies[depositEdited].additionalPayment == undefined){
            $scope.depositPolicies[depositEdited].additionalPayment = false;
        }

        if($scope.depositPolicies[depositEdited].additionalPayment.daysBefore && $scope.depositPolicies[depositEdited].additionalPayment){
            $scope.deposit.daysBefore = $scope.depositPolicies[depositEdited].additionalPayment.daysBefore;
            $scope.deposit.additionalPaymentAmount = $scope.depositPolicies[depositEdited].additionalPayment.amount;

               $scope.depositAdditionalPayment = true;

             //second additional payment
            if($scope.depositPolicies[depositEdited].additionalPayment1 == undefined){$scope.depositPolicies[depositEdited].additionalPayment1 = false}

            if($scope.depositPolicies[depositEdited].additionalPayment1.daysBefore && $scope.depositPolicies[depositEdited].additionalPayment1){
            $scope.deposit.daysBefore1 = $scope.depositPolicies[depositEdited].additionalPayment1.daysBefore;
            $scope.deposit.additionalPaymentAmount1 = $scope.depositPolicies[depositEdited].additionalPayment1.amount;

                $scope.depositAdditionalPayment1 = true;
            }
        }
        
        $scope.toggleDeposits('all');

    }

    $scope.addDeposit = function(instruction){
        
        //Check if name already exists 
        if(ifNameRepeated('deposit',$scope.deposit.policyName, instruction))
        {
            return alert('this policy name already exists');
        }

                  //validation    
                if(!(depositValidation($scope.deposit)))
                {
                    return alert('plese type your deposit form properly');
                }
      
                var currentTime = new Date();

                var oneDepositPolicy = 
                    {
                        "name": $scope.deposit.policyName,
                        "amount": $scope.deposit.amount
                    };

                //assign ID to each new object
                if(!instruction)
                {   
                    oneDepositPolicy.id =  ++ids.deposit;
                }
                else
                {   
                     oneDepositPolicy.id = $scope.depositPolicies[$scope.editDep-1].id;
                }

                // check if just created or only updated
                if(!instruction)
                {   
                    oneDepositPolicy.created = currentTime;
                    oneDepositPolicy.updated = currentTime;
                }
                else if(instruction)
                {
                    oneDepositPolicy.created = $scope.depositPolicies[$scope.editDep-1].created; 
                    oneDepositPolicy.updated = currentTime;
                }
                
                    if($scope.depositAdditionalPayment){   
                        oneDepositPolicy.additionalPayment = {};
                        oneDepositPolicy.additionalPayment.daysBefore = $scope.deposit.daysBefore;
                        oneDepositPolicy.additionalPayment.amount = $scope.deposit.additionalPaymentAmount;

                        if($scope.depositAdditionalPayment1)
                        {
                            oneDepositPolicy.additionalPayment1 = {};
                            oneDepositPolicy.additionalPayment1.daysBefore = $scope.deposit.daysBefore1;
                            oneDepositPolicy.additionalPayment1.amount = $scope.deposit.additionalPaymentAmount1;
                        }
                    }
                

               instruction ? $scope.depositPolicies[$scope.editDep-1] = oneDepositPolicy : $scope.depositPolicies.push(oneDepositPolicy);

               //save to local storage
               // $localStorage.depositPolicies = $scope.depositPolicies;
            $scope.cancelDeposit();
    };

    $scope.cancelDeposit = function(){
            $scope.editDep = null; 
            $scope.deposit.policyName = '';
            $scope.deposit.amount = 30;
            $scope.deposit.daysBefore = 0;
            $scope.deposit.daysBefore1 = 0;
            $scope.deposit.additionalPaymentAmount = 0;
            if($scope.depositAdditionalPayment){$scope.depositAdditionalPayment = !$scope.depositAdditionalPayment;}

            $scope.toggleDeposits('all');
    };

    $scope.deleteDeposit = function(number){
        $scope.depositPolicies.splice(number,1);
        //delete from local storage
        // delete $localStorage.depositPolicies;
        // $localStorage.depositPolicies = $scope.depositPolicies;
    };

    $scope.findDepositArray = function(indexOfArray){
        $scope.deposit.toDelete = indexOfArray;
    };
    //
    // CANCELLATIONS
    //

    $scope.cancellation = false;
    $scope.countRules = 1;
    $scope.cancellationForm = {};
    $scope.cancellationForm.toDelete = null;
    $scope.editedCancellation = null;

    $scope.toggleCancellations = function(){
       $scope.editedCancellation = false;
        $scope.cancellation = !$scope.cancellation;
        $scope.countRules = 1;
        $scope.cancellationForm = {};
    };

    $scope.addRule = function(){
        $scope.countRules++;
    };

    $scope.getRulesNumber = function(num){
        return new Array(num);
    };

    $scope.editCancellation = function(editedCanc){
        $scope.cancellation = true;
        $scope.editedCancellation = editedCanc;
        $scope.cancellationForm.policyName = $scope.cancellations[editedCanc].name;
        

        $scope.countRules = Object.keys($scope.cancellations[editedCanc].rule).length
            
            // clean models for edit form 
            $scope.cancellationForm.period = [];
            $scope.cancellationForm.amount = [];
            $scope.cancellationForm.type = [];

        for(var i = 0; i < $scope.countRules; i++){
            $scope.cancellationForm.period[i] = $scope.cancellations[editedCanc].rule[i].daysBefore;
            $scope.cancellationForm.amount[i] = $scope.cancellations[editedCanc].rule[i].fee;
            $scope.cancellationForm.type[i] =  $scope.cancellations[editedCanc].rule[i].type;
        }
    };

    $scope.deleteCancellation = function(i){

        $scope.cancellations.splice(i,1);
        //delete from local storage
        // delete $localStorage.cancellations;
        // $localStorage.cancellations = $scope.cancellations;

    };

    $scope.saveCancellation = function(instruction){
        //Check if name already exists 
        if(ifNameRepeated('cancellation',$scope.cancellationForm.policyName,instruction))
        {
            return alert('this policy name already exists');
        }
        //Check if saved or edited
        // if(instruction)
        // {
        //     $scope.cancellations.splice($scope.editedCancellation,1);
        //     $scope.editedCancellation = false;
        // }
        var currentTime = new Date();

        //validation
         if(!cancellationValidation($scope.cancellationForm))
        {
            return alert('please type your cancellation policy properly');
        }

        var oneCancellation = {};
        oneCancellation.name = $scope.cancellationForm.policyName;

        if(!instruction)
        {   
          oneCancellation.id =  ++ids.cancellation;
        }
        else
        {  
          oneCancellation.id = $scope.cancellations[$scope.editedCancellation].id;
        }

        // check if just created or only updated
        if(!instruction)
        {   
            oneCancellation.created = currentTime;
            oneCancellation.updated = currentTime;
        }
        else if(instruction)
        {   
            oneCancellation.created = $scope.cancellations[$scope.editedCancellation].created;
            oneCancellation.updated = currentTime;   
        }
       
        oneCancellation.rule = [];
    
        for(var i = 0; i < (Object.keys($scope.cancellationForm.period).length); i++){
            
            var rule = {};
            rule.daysBefore = $scope.cancellationForm.period[i];
            rule.fee = $scope.cancellationForm.amount[i];
            rule.type = $scope.cancellationForm.type[i];
            oneCancellation.rule.push(rule);
        };

        instruction ? $scope.cancellations[$scope.editedCancellation] = oneCancellation :  $scope.cancellations.push(oneCancellation);

        //save to local storage
        // $localStorage.cancellations = $scope.cancellations;

        $scope.toggleCancellations();

        };

    //
    //CHILDREN
    //

     $scope.kids = false;
     $scope.childrenForm = {};
     $scope.childrenForm.selectedAge = 0;
     $scope.childrenForm.selectedMinAgeRange = 0;

     $scope.watchChildAge = function(){
        if($scope.childrenForm.selectedAge == "none"){$scope.childrenForm.selectedMinAgeRange = 0}
        else{$scope.childrenForm.selectedMinAgeRange =  $scope.childrenForm.selectedAge};
     };

     $scope.addChildren = function(ifChildren){
        if(!ifChildren)
        {
             $scope.kids = false;
             delete $scope.children.policies;
             $scope.children.ifChildren = false;
             $('.yesChildren').removeClass('clickedRooms');
             $('.noChildren').addClass('clickedRooms');
        }
        else
        {   
            $scope.children.policies = [];
            $scope.children.ifChildren = true;
            $scope.kids = true;
            $('.noChildren').removeClass('clickedRooms');
            $('.yesChildren').addClass('clickedRooms');
        };
      };

      $scope.saveChildrenPolicy = function(instruction){
         //Validate and Check if name already exists 

         if(!("name" in $scope.childrenForm))
        {
            return alert('please type your children policy properly')
        }

        if($scope.childrenForm.name.length == 0)
        {
            return alert('please type your children policy properly');
        }
        
        if(ifNameRepeated('children',$scope.childrenForm.name,instruction))
        {
            return alert('this policy name already exists');
        }

          //check if save or edit
        // if(instruction)
        // {
        //     $scope.children.policies.splice($scope.childrenForm.policyToEdit,1);
        //     $scope.childrenForm.ifEdit = false;
        // };

        var currentTime = new Date();

        if(!$scope.children.ifChildren){
            delete $scope.children.ifChildren;
        };

        var oneChildrenPolicy = {};

         if(!instruction)
        {
            oneChildrenPolicy.created = currentTime;
            oneChildrenPolicy.updated = currentTime;
        }
        else if(instruction)
        {
            oneChildrenPolicy.created = $scope.children.policies[$scope.childrenForm.policyToEdit].created;
            oneChildrenPolicy.updated = currentTime;   
        }

        oneChildrenPolicy.name = $scope.childrenForm.name;
        oneChildrenPolicy.ageRange = $('#ageLimits').val();

        //assign id
        if(!instruction)
        {
            oneChildrenPolicy.id = ++ids.children;
        }
        else
        {
           oneChildrenPolicy.id = $scope.children.policies[$scope.childrenForm.policyToEdit].id;
        }


        instruction ? $scope.children.policies[$scope.childrenForm.policyToEdit] = oneChildrenPolicy :  $scope.children.policies.push(oneChildrenPolicy);

        //clean and close child policy form
        $scope.cancelChildPolicy();

        //save to local storage
        // $localStorage.children = $scope.children.policies;
        
      };

      $scope.cancelChildPolicy = function(){
        // hide children policy form and reset values
        $('.childrenCollapse').collapse('hide');
        // $('#ageLimits').val("0 - 17");
        $scope.childrenForm.name = null;
        $scope.childrenForm.ifEdit = false;
      };

      $scope.deleteChildPolicy = function(policyNumber){
        $scope.children.policies.splice(policyNumber,1);
         //delete from local storage
        //  delete $localStorage.children;
        // $localStorage.children = $scope.children.policies;
      };

      $scope.editChildPolicy = function(policyNumber){
        $scope.childrenForm.ifEdit = true;
        $scope.childrenForm.policyToEdit = policyNumber;
        $('.childrenCollapse').collapse('show');
        $scope.childrenForm.name = $scope.children.policies[policyNumber].name;
        // update age limit in form with value from edited policy
        $('#ageLimits').val($scope.children.policies[policyNumber].ageRange);
      };
      
      //
      // COMMON FUNCTIONS FOR VALIDATION AND SENDING
      //

      $scope.sendData = function()
      {
        // Check if all policies are not empty
        if($scope.children.ifChildren == undefined || $scope.depositPolicies.length == 0){
            return alert('please fullfill all policies');
        }

        if($scope.children.ifChildren == true)
        {
            delete $scope.children.ifChildren;
        }

        CommonJsonToSend.deposit =  $scope.depositPolicies;
        CommonJsonToSend.cancellation =  $scope.cancellations;
        CommonJsonToSend.children =  $scope.children;
        // console.log(JSON.stringify(CommonJsonToSend));
        console.log(JSON.stringify(CommonJsonToSend));
        // $location.path('/ratePlan');
      }

      function ifNameRepeated(data,name,edited){
        // reject call if policy is edited
        if(edited){return false};

        switch (data){
            case 'deposit':
                for(var i = 0; i < $scope.depositPolicies.length; i++){
                    if($scope.depositPolicies[i].name == name){
                        return true;
                    }
                };
                break;
            case 'cancellation':

                for(var i = 0; i < $scope.cancellations.length; i++){
                    if($scope.cancellations[i].name == name){
                        return true;
                    }
                };
                break;
            case 'children':
                for(var i = 0; i < $scope.children.policies.length; i++){
                    if($scope.children.policies[i].name == name){
                        return true;
                     }
                };
                break;
            }
            return false;
      } 

      // validation
      function depositValidation(depositPolicy)
      {
        if(depositPolicy.policyName.length == 0){
            return false;
        }
        
        if(typeof(depositPolicy.additionalPayment) != 'undefined')
        {   
          
            if(depositPolicy.additionalPayment.amount <= 0){
                return false;
            }
            if(depositPolicy.additionalPayment.daysBefore <= 0){
                return false;
            }
        }

         if(typeof(depositPolicy.additionalPayment1) != 'undefined')
        {
            if(depositPolicy.additionalPayment1.amount <= 0){
                return false;
            }
            if(depositPolicy.additionalPayment1.daysBefore <= 0){
                return false;
            }
        }

            return true;
      }

      function cancellationValidation(cancellationPolicy)
      {
        if(typeof(cancellationPolicy.policyName) == 'undefined'){return false};

        if(cancellationPolicy.policyName.length == 0){return false};
        
        if(!("type" in cancellationPolicy) || !("amount" in cancellationPolicy) || !("period" in cancellationPolicy)){return false};

        for(var i = 0; i < $scope.countRules; i++)
        {   
            
            if(cancellationPolicy.amount[i] <= 0 || cancellationPolicy.period[i] <= 0 || typeof(cancellationPolicy.type[i]) == 'undefined' )
            {
                return false;
            }
        }

        //return true if object will pass validation process succesfully 
        return true;
      }
}]);






