var app = angular.module('onboarding',['ngRoute','ngSanitize']);

app.config(function($routeProvider){
	$routeProvider
	.when('/',{
		templateUrl: '/ratePlan.html',
		controller: 'ratePlanController'
	})
	 .otherwise({redirectTo:'/'});
	
});