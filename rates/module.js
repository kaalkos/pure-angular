var app = angular.module('onboarding',['ngRoute','ngSanitize']);

app.config(function($routeProvider){
	$routeProvider
	.when('/',{
		templateUrl: '/rates.html',
		controller: 'ratesController'
	})
    
	 .otherwise({redirectTo:'/'});
	
});