app.controller('ratesController',['$scope','$http', '$filter', '$window', function($scope,$http,$filter,$window){
	//
	//main array
	//
	$scope.rateRulePlan = [];
	
	$scope.ratePolicy = {};

	$scope.ratePlansRelated = [];
	$scope.inheritedRatePlans = [];
	$scope.newRule = {};
	$scope.visibility = false;
	$scope.rateRulesVisible = true;

	$scope.newRule.minStay = 1;
	$scope.newRule.maxStay = 999;

	$scope.newRule.days = [];
	$scope.newRule.daysArrival = [];
	$scope.newRule.daysDeparture = [];

	var allRoms = true;
	var allDays = true;
	var allDaysArrival = true;
	var allDaysDeparture = true;
	var stopSale = true;

	// load fake rooms and rate plans from json files
	$http.get('rooms.json').then(function(res){
		$scope.rooms = res.data;
		fetchRatePlans();
	});

	function fetchRatePlans(){
		$http.get('ratePlans.json').then(function(res){
			$scope.ratePlans = res.data;
			// call assing function after data loading 
			assignRatePlans();
		});
	}

	function assignRatePlans(){

		for(var i = 0; i < Object.keys($scope.rooms).length; i++){
			$scope.ratePlansRelated[i] = {};

 			for(n = 0; n < Object.keys($scope.ratePlans).length; n++){
					
						for(var k = 0; k < $scope.ratePlans[n].rooms.length; k++){
							if($scope.ratePlans[n].rooms[k].name == $scope.rooms[i].name)
							{	
								$scope.ratePlansRelated[i][n] = {};
								$scope.ratePlansRelated[i][n].name = $scope.ratePlans[n].name;
								// $scope.ratePlansRelated[i][n].rateRules = [];
								$scope.ratePlansRelated[i][n].per = $scope.ratePlans[n].per;
							}
						}
					
			}
		}
		//colect indexes of inherited rate plans
		for(var i = 0; i < Object.keys($scope.ratePlans).length; i++){
			if($scope.ratePlans[i]['inherit'] != undefined){
				$scope.inheritedRatePlans.push(i);
			}
		}
		// display view only when data will be ready
		$scope.visibility = true;
		
	}

	$scope.ifInherited = function(index){
		for(var i = 0; i < $scope.inheritedRatePlans.length; i++){
			if($scope.inheritedRatePlans[i] == index)
			{
				return true;
			}
		}
		return false;
	}

	$scope.assignDays = function(type,day){
		switch (type) {
			case 'days':
				if(day == 'all'){
					if(allDays)
					{
						$scope.newRule.days = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
						// change color of the buttons
						$('.days').each(function(){
							$(this).find(':button').addClass('clickedRooms');
						});
						allDays = !allDays;
					}
					else
					{
						$('.days').each(function(){
							$(this).find(':button').removeClass('clickedRooms');
						});
						$scope.newRule.days = [];
						allDays = !allDays;
					}
				}
				else{
					if(typeof($scope.newRule.days) != "object"){$scope.newRule.days = []};
					$('.days').find(':button').eq(0).removeClass('clickedRooms');
					$('.days:contains('+day+')').find(':button').toggleClass('clickedRooms');

					allDays = true;

					for(var i = 0; i < $scope.newRule.days.length; i++){
						if($scope.newRule.days[i] == day){
							$scope.newRule.days.splice(i,1);
							return false;
						}
					}
					$scope.newRule.days.push(day);

					if($scope.newRule.days.length == 7){
					$('.days').find(':button').eq(0).addClass('clickedRooms');
					allDays = !allDays;
					}
				}
				break;
			case 'arrival':
				if(day == 'all'){
					if(allDaysArrival)
					{
						$scope.newRule.daysArrival = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
						// change color of the buttons
						$('.daysArrival').each(function(){
							$(this).find(':button').addClass('clickedRooms');
						});
						allDaysArrival = !allDaysArrival;
					}
					else
					{
						$('.daysArrival').each(function(){
							$(this).find(':button').removeClass('clickedRooms');
						});
						$scope.newRule.daysArrival = [];
						allDaysArrival = !allDaysArrival;

					}
				}
				else{
					if(typeof($scope.newRule.daysArrival) != "object"){$scope.newRule.daysArrival = []};
					$('.daysArrival').find(':button').eq(0).removeClass('clickedRooms');
					$('.daysArrival:contains('+day+')').find(':button').toggleClass('clickedRooms');

					allDaysArrival = true;

					for(var i = 0; i < $scope.newRule.daysArrival.length; i++){
						if($scope.newRule.daysArrival[i] == day)
						{
							$scope.newRule.daysArrival.splice(i,1);
							return false;
						}
					}
					$scope.newRule.daysArrival.push(day);
					if($scope.newRule.daysArrival.length == 7){
					$('.daysArrival').find(':button').eq(0).addClass('clickedRooms');
					allDaysArrival = !allDaysArrival;
					}
				}
				break;
			case 'departure':
				if(day == 'all'){
					if(allDaysDeparture)
					{
						$scope.newRule.daysDeparture = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
						// change color of the buttons
						$('.daysDeparture').each(function(){
							$(this).find(':button').addClass('clickedRooms');
						});
						allDaysDeparture = !allDaysDeparture;
					}
					else
					{
						$('.daysDeparture').each(function(){
							$(this).find(':button').removeClass('clickedRooms');
						});
						$scope.newRule.daysDeparture = [];
						allDaysDeparture = !allDaysDeparture;
					}
				}
				else{
					if(typeof($scope.newRule.daysDeparture) != "object"){$scope.newRule.daysDeparture = []};
					$('.daysDeparture').find(':button').eq(0).removeClass('clickedRooms');
					$('.daysDeparture:contains('+day+')').find(':button').toggleClass('clickedRooms');

					allDaysDeparture = true;

					for(var i = 0; i < $scope.newRule.daysDeparture.length; i++){
						if($scope.newRule.daysDeparture[i] == day){
							$scope.newRule.daysDeparture.splice(i,1);
							return false;
						}
					}
					$scope.newRule.daysDeparture.push(day);
					if($scope.newRule.daysDeparture.length == 7){
					$('.daysDeparture').find(':button').eq(0).addClass('clickedRooms');
					allDaysDeparture = !allDaysDeparture;
					}
				}
				break;
			case 'stopSale':
				if(day == 'all'){
					if(stopSale)
					{
						$scope.newRule.stopSale = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
						// change color of the buttons
						$('.daysStopSale').each(function(){
							$(this).find(':button').addClass('clickedRooms');
						});
						stopSale = !stopSale;
					}
					else
					{
						$('.daysStopSale').each(function(){
							$(this).find(':button').removeClass('clickedRooms');
						});
						$scope.newRule.stopSale = [];
						stopSale = !stopSale;
					}
				}
				else{
					if(typeof($scope.newRule.stopSale) != "object"){$scope.newRule.stopSale = []};
					$('.daysStopSale').find(':button').eq(0).removeClass('clickedRooms');
					$('.daysStopSale:contains('+day+')').find(':button').toggleClass('clickedRooms');

					stopSale = true;

					for(var i = 0; i < $scope.newRule.stopSale.length; i++){
						if($scope.newRule.stopSale[i] == day){
							$scope.newRule.stopSale.splice(i,1);
							return false;
						}
					}
					$scope.newRule.stopSale.push(day);
					if($scope.newRule.stopSale.length == 7){
					$('.daysStopSale').find(':button').eq(0).addClass('clickedRooms');
					stopSale = !stopSale;
					}
				}
				break;
		}
	}

	$scope.assignRoom = function(instruction){
		switch (instruction){
			case 'all':
				$scope.newRule.rooms = [];
				for(var i = 0; i < Object.keys($scope.rooms).length; i++){
					$scope.newRule.rooms.push($scope.rooms[i].name);
				}
				$('.addRoom').removeClass('clickedRooms');
				$('.addRoom').eq(1).addClass('clickedRooms');
				$scope.visibleRooms = false;
				break;
			case 'one':
				$scope.newRule.rooms = $scope.newRule.roomName;
				$('.addRoom').removeClass('clickedRooms');
				$('.addRoom').eq(0).addClass('clickedRooms');
				$scope.visibleRooms = false;
				break;
			case 'some':
				$scope.newRule.rooms = []
				$('.addRoom').removeClass('clickedRooms');
				$('.addRoom').eq(2).addClass('clickedRooms');
				$scope.visibleRooms = true;
		}
	}

	$scope.assignRates = function(instruction){
		switch (instruction){
			case 'all':
				$scope.newRule.rates = [];
				for(var i = 0; i < Object.keys($scope.ratePlans).length; i++){
					$scope.newRule.rates.push($scope.ratePlans[i].name);
				}
				$('.addRate').removeClass('clickedRooms');
				$('.addRate').eq(1).addClass('clickedRooms');
				$scope.visibleRates = false;
				break;
			case 'one':
				$scope.newRule.rates = $scope.newRule.ratePlanName;
				$('.addRate').removeClass('clickedRooms');
				$('.addRate').eq(0).addClass('clickedRooms');
				$scope.visibleRates = false;
				break;
			case 'some':
				$scope.newRule.rates = []
				$('.addRate').removeClass('clickedRooms');
				$('.addRate').eq(2).addClass('clickedRooms');
				$scope.visibleRates = true;
		}
	}

	$scope.certainRoom = function(roomTypeName){
		for(var i = 0; i < $scope.newRule.rooms.length; i++){
			if($scope.newRule.rooms[i] == roomTypeName){
				$scope.newRule.rooms.splice(i,1);
				$('.certainRoom:contains('+roomTypeName+')').removeClass('clickedRooms');
				return false;
			}
		}
		$scope.newRule.rooms.push(roomTypeName);
		$('.certainRoom:contains('+roomTypeName+')').addClass('clickedRooms');
	}

	$scope.certainRate = function(rateTypeName){
		for(var i = 0; i < $scope.newRule.rates.length; i++){
			if($scope.newRule.rates[i] == rateTypeName){
				$scope.newRule.rates.splice(i,1);
				$('.certainRate:contains('+rateTypeName+')').removeClass('clickedRooms');
				return false;
			}
		}
		$scope.newRule.rates.push(rateTypeName);
		$('.certainRate:contains('+rateTypeName+')').addClass('clickedRooms');
	}

	function getRoomPrice(name){
		for(var i = 0; i < $scope.rooms.length; i++){
			if($scope.rooms[i].name == name)
			{
				return $scope.rooms[i].baseRate;
			}
		}
	}

	$scope.cleanModal = function(){
		
		$scope.newRule.name = null;
		$scope.newRule.seasonFrom = null;
		$scope.newRule.seasonTo = null;
		$scope.newRule.stopSale = null;
		$scope.newRule.minStay = 1;
		$scope.newRule.maxStay = 999;
		$scope.newRule.daysArrival = $scope.newRule.days = $scope.newRule.daysDeparture = [];
		$('.addRate').removeClass('clickedRooms');
		$('.addRoom').removeClass('clickedRooms');
		$('.days, .daysDeparture, .daysArrival, .daysStopSale').find(':button').removeClass('clickedRooms');

	}
	//
  	//
  	// SEASONS
  	//
  	//
  	$scope.seasons = [];
  	$scope.seasonForm = {};
 
  	$scope.seasonForm.visible = false;

  	

  	$scope.openSeasons = function(){

  		$scope.seasonForm.from = new Date();
  		var plusMonth = new Date();
  		plusMonth.setMonth(plusMonth.getMonth() + 1);
  		$scope.seasonForm.to = new Date(plusMonth);

  		$("#seasonsModal").modal();
  	}

  	$scope.saveSeason = function(name,seasonFrom,seasonTo){
  		
  		//clean add season form from 'apply to more dates' collapse window
  		$scope.newRule.seasonName = $scope.newRule.startDate = $scope.newRule.endDate = null;
 		var seasonForm = {};
  	
  		if(!checkDate(seasonFrom,seasonTo)){
  			alert('please type your date properly');
  			return false;
  		}
  		for(var i in $scope.seasons){
  			if($scope.seasons[i].name == name)
  			{
  				alert('this season name already exists');
  				return false;
  			}
  		}
  		seasonForm.from = seasonFrom;
  		seasonForm.to = seasonTo;
  		seasonForm.name = name;
  		seasonForm.isCollapse = false;

  		$scope.seasons.push(seasonForm);
  		
  		$scope.cleanSeasonModal(false);
  	};

  	$scope.cleanSeasonModal = function(close){
  		if(close){$("#seasonsModal").modal('hide');}
  		$scope.seasonForm.visible = false;
  		$scope.seasonForm.from = null;
  		$scope.seasonForm.name = null
  		$scope.seasonForm.to = null;
  		// clean all edit season collapses
  		for(var i in $scope.seasons){
  			$scope.seasons[i].isCollapse = false;
  		}
  		
  	}

  	$scope.editSeason = function(prevName, Newname,seasonFrom,seasonTo){
  		
  		if(!checkDate(seasonFrom,seasonTo)){
  			alert('please type your date properly');
  			return false;
  		}

  		for(var i in $scope.seasons){
  			if($scope.seasons[i].name == prevName)
  			{
  				$scope.seasons[i].name = Newname;
  				$scope.seasons[i].from = seasonFrom;
  				$scope.seasons[i].to = seasonTo;
  				$scope.seasons[i].isCollapse = false;
  				return true;
  			}
  		}
  	}

  	$scope.deleteSeason = function(index){
  		$scope.seasons.splice(index,1);
  	}

  	function checkDate(fromDate,toDate){

  		if(toDate <= fromDate){
  			return false;
  		}
  		else if(toDate > fromDate){
  			return true;
  		}
  	}
  	//
  	//
  	// RATE PLAN EDIT
  	//
  	//
  	var roomTypes = [];
  	$scope.ratePlanForm = {};
  	var ratePlanIndex;
  	
  	// download policies for edit panel from external file
  	$http.get('policies.json').then(function(res){
		$scope.policies = res.data
	});

	$scope.openRatePlan = function(name){
		//assing the name for the header
		$scope.ratePlanHeadline = name; 
		// assign rate plan to the form
		for(var i in $scope.ratePlans){
			if($scope.ratePlans[i].name == name){
				$scope.ratePlanForm = $scope.ratePlans[i];
				ratePlanIndex = i;
			}
		}
	
		roomTypes = [];
		for(var i in $scope.rooms){
					for(var n in $scope.ratePlanForm.rooms){
						if($scope.ratePlanForm.rooms[n].name == $scope.rooms[i].name){
							roomTypes.push(parseInt(i));
						}
					}
				}

		// clean all marked buttons
		$('.depositPolicy, .mealPolicy, .cancellationPolicy, .perPolicy, .roomsBtn, #allRoomsBtn').removeClass('clickedRooms');
		//assign colors to buttons of choosen policies
		$('.depositPolicy:contains('+$scope.ratePlanForm.deposit.name+')').addClass('clickedRooms');
		$('.mealPolicy:contains('+$scope.ratePlanForm.meal+')').addClass('clickedRooms');
		$('.cancellationPolicy:contains('+$scope.ratePlanForm.cancellation.name+')').addClass('clickedRooms');
		
		$('.perPolicy:contains('+$scope.ratePlanForm.per+')').addClass('clickedRooms');

		
			for(var i  = 0; i < Object.keys($scope.ratePlanForm.rooms).length; i++){
				$('.roomsBtn:contains('+$scope.ratePlanForm.rooms[i].name+')').addClass('clickedRooms');
			}

			allRoms = true;

			if($scope.ratePlanForm.rooms.length == $scope.rooms.length){
				$('#allRoomsBtn').addClass('clickedRooms');
				allRoms = false;
			}
		
		$('.roomsBtn').each(function(){

		});
		// open edit window
		$scope.rateRulesVisible = false;
		//scroll to the top
		$window.scrollTo(0, 0);
	}


	$scope.toggleRatePlan = function(instruction){
		$scope.rateRulesVisible = instruction;
		$window.scrollTo(0, 0);
	}

	$scope.chooseRoom = function(roomNum){

		if(roomNum == 'all'){

			roomTypes = [];

			if(allRoms){

				$('#allRoomsBtn,.roomsBtn').addClass('clickedRooms');
				for(var i in $scope.rooms){
					roomTypes.push(parseInt(i));
				}
				allRoms = !allRoms;
			}
			else
			{	
				$('#allRoomsBtn,.roomsBtn').removeClass('clickedRooms');
				allRoms = !allRoms;
			}
						
		}
		else{	
			if(typeof(roomTypes) == 'string'){roomTypes = []};
			$('#allRoomsBtn').removeClass('clickedRooms');
			$('.roomsBtn').eq(roomNum).toggleClass('clickedRooms');
			allRoms = true;

			for(var i = 0; i < roomTypes.length;i++){
				if(roomTypes[i] === roomNum){
					roomTypes.splice(i,1);
					
					return false;
				}
			}
			roomTypes.push(roomNum);
			if(roomTypes.length == $scope.rooms.length)
			{
				$('#allRoomsBtn').addClass('clickedRooms');
				allRoms = false;
			}
		}
		
	}


	$scope.roomRatePlan = function(per){
		$scope.ratePlanForm.per = per;
		if(per === "room"){
			$('#person').removeClass('clickedRooms');
			$('#room').addClass('clickedRooms');
		}
		else if(per === "person"){
			$('#room').removeClass('clickedRooms');
			$('#person').addClass('clickedRooms');
		}
	}


	$scope.assingRatePolicy = function(policy,index){
			if(policy === 'deposit'){
				$scope.ratePlanForm.deposit = $scope.policies.deposit[index];
				// change color of the clicked policy
				$('.depositPolicy').removeClass('clickedRooms');
				$('.depositPolicy').eq(index).addClass('clickedRooms');
			}
			else if(policy === 'cancellation'){
				$scope.ratePlanForm.cancellation = $scope.policies.cancellation[index];
				// change color of the clicked policy
				$('.cancellationPolicy').removeClass('clickedRooms');
				$('.cancellationPolicy').eq(index).addClass('clickedRooms');
			}
			else if(policy === 'meal'){
				$('.mealPolicy').removeClass('clickedRooms');
				$('.mealPolicy').eq(index).addClass('clickedRooms');
			}
			
	}

	$scope.editRatePlan = function(){
		
		if($scope.ratePlanForm.name.length == 0)
		{
			return alert('please type rate plan properly');
		}

		// assign rooms by indexes from roomTypes array
		if(roomTypes == 'all'){$scope.ratePlanForm.rooms = roomTypes}
		else{
			$scope.ratePlanForm.rooms = [];
			for(var i in roomTypes){
				$scope.ratePlanForm.rooms.push($scope.rooms[roomTypes[i]]);
			}
			
		}
		$scope.ratePlans[ratePlanIndex] = $scope.ratePlanForm;

		// close edit window and scroll top
		assignRatePlans();
		$scope.rateRulesVisible = true;
		$window.scrollTo(0, 0);

	}

	//
	// CALENDAR
	//
	$scope.currentDate = {};
	$scope.currentDate.date = new Date();
	
	$scope.myDates = [];
	$scope.displayedDates = [];

	$scope.currentlyOpened;

	// number of index to display
	var calendarIndex = {};
	
	var lastDate;

	var onlyGenerate = false;

	var dateOnCalendar = $scope.currentDate.date;

	var daysOfWeek = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];

	var calendarInstruction = {};

	var alreadyGenerated = {};

	var id = 1;

	$scope.checkIt = function(rateType)
	{	
	
		for(var key in $scope.ratePolicy){
			$scope.ratePolicy[key] = false;
		}
		$scope.ratePolicy[rateType] = true;
		$scope.currentlyOpened = rateType;
		
	}

	// yeah... it seems to be quite dumb and it breaks the DRY rule... however since we have counted number of objects watching each one of them 
	// separatly is much faster and more convinient way then watching whole huge array of objects. Also it's easier to recognize which
	// date has been changed. 

	$scope.$watchCollection('displayedDates[0]',function(newVal, oldVal){

		if(newVal !== oldVal)
		{
			saveDate(newVal);
		}

	});
	$scope.$watchCollection('displayedDates[1]',function(newVal, oldVal){
	
		if(newVal !== oldVal)
		{
			saveDate(newVal);
		}

	});
	$scope.$watchCollection('displayedDates[2]',function(newVal, oldVal){
	
		if(newVal !== oldVal)
		{
			saveDate(newVal);
		}

	});
	$scope.$watchCollection('displayedDates[3]',function(newVal, oldVal){
	
		if(newVal !== oldVal)
		{
			saveDate(newVal);
		}

	});
	$scope.$watchCollection('displayedDates[4]',function(newVal, oldVal){
	
		if(newVal !== oldVal)
		{
			saveDate(newVal);
		}

	});
	$scope.$watchCollection('displayedDates[5]',function(newVal, oldVal){
	
		if(newVal !== oldVal)
		{
			saveDate(newVal);
		}

	});
	$scope.$watchCollection('displayedDates[6]',function(newVal, oldVal){
	
		if(newVal !== oldVal)
		{
			saveDate(newVal);
		}

	});
	$scope.$watchCollection('displayedDates[7]',function(newVal, oldVal){
	
		if(newVal !== oldVal)
		{
			saveDate(newVal);
		}

	});
	$scope.$watchCollection('displayedDates[8]',function(newVal, oldVal){
		
		if(newVal !== oldVal)
		{
			saveDate(newVal);
		}

	});
	$scope.$watchCollection('displayedDates[9]',function(newVal, oldVal){
	
		if(newVal !== oldVal)
		{
			saveDate(newVal);
		}

	});

	function saveDate(dateObject)
	{	

	
		if(typeof(dateObject) != "undefined")
		{	
			
			for(var i in $scope.myDates[$scope.currentlyOpened])
			{	
				if(Object.keys($scope.myDates[$scope.currentlyOpened][i])[0] == dateObject.realDate)
				{
					$scope.myDates[$scope.currentlyOpened][i][dateObject.realDate] = dateObject;
				}
			}
			
		}

	}

	$scope.browseCalendar = function(direction,days,roomName){
				

		if(direction == 'right')
		{	
				if(calendarInstruction[roomName] + days  > $scope.myDates[roomName].length)
				{
					$scope.generateDays(false,days,roomName);
					calendarInstruction[roomName] += days;
				}
				else if(calendarInstruction[roomName] + days  <= $scope.myDates[roomName].length)
				{
					calendarInstruction[roomName] += days;
					displayDays(calendarInstruction[roomName]-10,roomName);
				}
		}

		else if(direction == 'left')
		{	
			 if(calendarInstruction[roomName] > days)
			 {
			 	
			 	if($scope.myDates[roomName].length == 10 || (calendarInstruction[roomName] - days - 10 < 0))
			 	{
			 		displayDays(0,roomName);
			 		calendarInstruction[roomName] = 10;
			 		return true;
			 	}

			 	calendarInstruction[roomName] -= days;
			 	displayDays(calendarInstruction[roomName]-10,roomName);

			 }
			 else
			 {
			 	displayDays(0,roomName);
			 	calendarInstruction[roomName] = 10;
			 }
		}


	}
	

	$scope.generateDays = function(current,days,roomName){

			if(calendarInstruction[roomName] == undefined)
			{
			calendarInstruction[roomName] = 10;
			}
			if(calendarIndex[roomName] == undefined)
			{
			calendarIndex[roomName] = 0;
			}
			
			//generate array of object for the ceratain rooms
			if($scope.myDates[roomName] == undefined)
			{	
				$scope.myDates[roomName] = [];
			}
			
			if(current)
			{	
				calendarIndex[roomName] = 0;
				// if(alreadyGenerated[roomName] == undefined)
				// {
				// 	alreadyGenerated[roomName] = true;

					var date = new Date();
					var start = new Date();
					var end = new Date();
				
					// start.setDate(date.getDate());
					
					// set how many days must be generate by loop
					end.setDate(date.getDate() + days);

					//save end date and use it when generate dates next time
					lastDate = end;
				// }
			}
			else
			{	
				var end = new Date(lastDate);
				var start = lastDate;
				end.setDate(start.getDate() + days);
				
				lastDate = end;
				calendarIndex[roomName] += days;
			}
			
			for(var d = start; d < end; d.setDate(d.getDate() + 1))
			{	
				var temp = {};
				var date = new Date(d);
				var day = daysOfWeek[date.getDay()];

				date = date.toISOString().slice(0, 10);
				
				temp[date] = {};

				temp[date].dayOfWeek = day;
				//price per room 
				temp[date].price = 100;
				//prices per person
				temp[date].adultMin = 100;
				temp[date].adultStd = 100;
				temp[date].adultAdditional = 100;
				temp[date].childAdditional = 100;

				temp[date].lengthOfStay = "1-999";
				temp[date].closedToArrival = false;
				temp[date].closedToDeparture = false;
				temp[date].stopSale = false;

				//assing id of each object
				temp[date].id = id++;

				$scope.myDates[roomName].push(temp);

			}
				
			return displayDays(calendarIndex[roomName],roomName);
	}


	function displayDays(index,roomName)
	{	
		// hide tooltip
		
		$('.toolTiptext').css('visibility','hidden');
		var n = index;

		if(onlyGenerate)
		{
			onlyGenerate = false;
			n = 0;
			// return false;
		}
		
		//set date displayed on calendar
		console.log($scope.myDates[roomName]);
		var calDate = (Object.keys($scope.myDates[roomName][n])[0]).split('-');
		
		$scope.currentDate.date = new Date(calDate[0],calDate[1]-1,calDate[2]);
		
		
		// assing days from general arrays to the current array of displayed
		$scope.displayedDates = [];

		for(var i = 0; i < 10; i++)
		{
			var temp = {};
			temp.realDate = Object.keys($scope.myDates[roomName][n+i])[0];
			temp.dayOfWeek = $scope.myDates[roomName][n+i][temp.realDate].dayOfWeek;
			temp.day = temp.realDate.slice(-2);
			temp.price = $scope.myDates[roomName][n+i][temp.realDate].price;

			temp.adultMin = $scope.myDates[roomName][n+i][temp.realDate].adultMin;
			temp.adultStd = $scope.myDates[roomName][n+i][temp.realDate].adultStd;
			temp.adultAdditional = $scope.myDates[roomName][n+i][temp.realDate].adultAdditional;
			temp.childAdditional = $scope.myDates[roomName][n+i][temp.realDate].childAdditional;

			temp.lengthOfStay = $scope.myDates[roomName][n+i][temp.realDate].lengthOfStay;
			temp.closedToArrival = $scope.myDates[roomName][n+i][temp.realDate].closedToArrival;
			temp.closedToDeparture = $scope.myDates[roomName][n+i][temp.realDate].closedToDeparture;
			temp.stopSale = $scope.myDates[roomName][n+i][temp.realDate].stopSale;

			$scope.displayedDates.push(temp);
		}

		
		
	}
		
	function countDays(date1,date2){
		var start =  new Date(date1.setHours(0,0,0));
		var end = date2;

		// convert dates into seconds and count the difference
		date1_unixtime = parseInt(start.getTime() / 1000);
		date2_unixtime = parseInt(end.getTime() / 1000);

		var timeDiff = date2_unixtime - date1_unixtime;

		var timeDiffHours = timeDiff / 60 / 60;

		var timeDiffDays = timeDiffHours / 24;
		
		return Math.ceil(timeDiffDays);
	}
	
	$scope.changeDate = function(endPointTime,roomName){

		if(endPointTime == null){return false};

		var distanceOfDays = countDays(dateOnCalendar,endPointTime);
		
		if(distanceOfDays > 0)
		{
			$scope.browseCalendar('right',distanceOfDays,roomName);
		}
		else
		{
			distanceOfDays = -distanceOfDays;
			$scope.browseCalendar('left',distanceOfDays,roomName);
		}

		dateOnCalendar = endPointTime;

	}

	$scope.moreDates = function(planName,roomName){
		
		// per room or per person
		for(var i = 0; i < Object.keys($scope.ratePlans).length;i++){
			if($scope.ratePlans[i].name == planName){
				if($scope.ratePlans[i].per == 'room'){
					$scope.newRule.per = "room";
				}
				else{
					$scope.newRule.per = "person";
				}
			}
		}	

		$scope.newRule.roomName = roomName;
		$scope.newRule.ratePlanName = planName;
		$scope.newRule.price = getRoomPrice(roomName);
		
		// set current date and one month forward 
		$scope.newRule.startDate = new Date();
		var plusMonth = new Date();
		plusMonth.setMonth(plusMonth.getMonth() + 1);
		$scope.newRule.endDate  = new Date(plusMonth);
	
		$("#ratesModal").modal();
	}

	$scope.imposeDates = function(instruction,roomName)
	{	
		var season;
		var distance;

		//find season object by its name
		for(var i in $scope.seasons){
			if($scope.seasons[i].name == $scope.newRule.selectedSeason)
			{
				season = $scope.seasons[i];
				break;
			}
		}

		var date1 = new Date(season.from);
		var date2 = new Date(season.to);

		// season.from = date1.toLocaleDateString().replace(".","-");
		// season.to = date2.toLocaleDateString().replace(".","-");
		season.from = date1.toISOString().slice(0,10);
		season.to = date2.toISOString().slice(0,10);

		// find if season is already within generated calendar
		for(var i in $scope.myDates[roomName]){
			
			if(Object.keys($scope.myDates[roomName][i])[0] == season.from)
			{	
				
				var startIndex = i;
				for(var n in $scope.myDates[roomName])
				{
					if(Object.keys($scope.myDates[roomName][n])[0] == season.to)
					{	
						imposeDatesOnCalendar(startIndex, n,instruction,roomName);
						return displayDays(0,roomName);
					}
				}
			}
		}
		//if not, generate days and then impose policies for them
		var lastIndex;

		var lastLength = ($scope.myDates[roomName].length-1);

		var lastExisted = Object.keys($scope.myDates[roomName][lastLength]);


		var fromDate = new Date(lastExisted);
		var toDate = new Date(season.to);

		onlyGenerate = true;

		distance = countDays(fromDate,toDate);

		$scope.generateDays(false,distance,roomName);

		for(var i in $scope.myDates[roomName])
		{
			if(Object.keys($scope.myDates[roomName][i]) == season.from)
			{
				lastIndex = parseInt(i);
				break;
			}
		}
		
		if(lastIndex == 0)
		{
			distance += 8;
		}
		imposeDatesOnCalendar(lastIndex, lastIndex+distance,instruction,roomName);
		return displayDays(0,roomName);


	}

	function imposeDatesOnCalendar(startIndex,endIndex,instruction,roomName)
	{	
		// choose method
		if(instruction == 'room')
		{	
			for(var i = startIndex; i <= endIndex; i++)
			{	
				var temp = Object.keys($scope.myDates[roomName][i])[0];
				
				if($scope.newRule.days.indexOf($scope.myDates[roomName][i][temp].dayOfWeek) != -1)
				{	
					$scope.myDates[roomName][i][temp].price = $scope.newRule.price;
					$scope.myDates[roomName][i][temp].lengthOfStay = $scope.newRule.minStay  + '-' + $scope.newRule.maxStay;

				}
				$scope.myDates[roomName][i][temp].closedToDeparture = false;
				$scope.myDates[roomName][i][temp].closedToArrival = false;
				$scope.myDates[roomName][i][temp].stopSale = false;

				if($scope.newRule.daysDeparture.indexOf($scope.myDates[roomName][i][temp].dayOfWeek) != -1)
				{
					$scope.myDates[roomName][i][temp].closedToDeparture = true;	
				}
				if($scope.newRule.daysArrival.indexOf($scope.myDates[roomName][i][temp].dayOfWeek) != -1)
				{
					$scope.myDates[roomName][i][temp].closedToArrival = true;		
				}
				if($scope.newRule.stopSale.indexOf($scope.myDates[roomName][i][temp].dayOfWeek) != -1)
				{
					$scope.myDates[roomName][i][temp].stopSale = true;		
				}
			}
		}
		else if(instruction == 'person')
		{	
			for(var i = startIndex; i <= endIndex; i++)
			{	
				var temp = Object.keys($scope.myDates[roomName][i])[0];
				
				if($scope.newRule.days.indexOf($scope.myDates[roomName][i][temp].dayOfWeek) != -1)
				{	
					$scope.myDates[roomName][i][temp].adultMin = $scope.newRule.adultMin;
					$scope.myDates[roomName][i][temp].adultStd = $scope.newRule.adultStd;
					$scope.myDates[roomName][i][temp].adultAdditional = $scope.newRule.adultAdditional;
					$scope.myDates[roomName][i][temp].childAdditional = $scope.newRule.childAdditional;

					$scope.myDates[roomName][i][temp].lengthOfStay = $scope.newRule.minStay  + '-' + $scope.newRule.maxStay;

					$scope.myDates[roomName][i][temp].closedToDeparture = false;
					$scope.myDates[roomName][i][temp].closedToArrival = false;
					$scope.myDates[roomName][i][temp].stopSale = false;

					if($scope.newRule.daysDeparture.indexOf($scope.myDates[roomName][i][temp].dayOfWeek) != -1)
					{
						$scope.myDates[roomName][i][temp].closedToDeparture = true;	
					}
					if($scope.newRule.daysArrival.indexOf($scope.myDates[roomName][i][temp].dayOfWeek) != -1)
					{
						$scope.myDates[roomName][i][temp].closedToArrival = true;		
					}
					if($scope.newRule.stopSale.indexOf($scope.myDates[roomName][i][temp].dayOfWeek) != -1)
					{
						$scope.myDates[roomName][i][temp].stopSale = true;		
					}

				}	

			}
		
		}
		
		
		// clean and hide modal
		$("#ratesModal").modal('hide');
		
		$scope.cleanModal();

	}

	$scope.activateTooltip = function(value,num,rateName){
		if(value == null){return false};

		
		$scope.newRule.stay = value.split('-');

		$('.toolTiptext').css('visibility','hidden');
		var classLength = ($('.toolTiptext').length)/10;
		
		var n = num;

		for(var i = 0; i < classLength; i++)
		{	
			if($('.toolTiptext').eq(n).find('span').text() == rateName)
			{
				
				$('.toolTiptext').eq(n).css('visibility','visible');
			}
			n += 10;
			
		}
	}

	$scope.watchStay = function(number)
	{	

		var minLength = Number($scope.newRule.stay[0]);
		var maxLength = Number($scope.newRule.stay[1]);

		if(isNaN(minLength) || $scope.newRule.stay[0].includes('.') )
		{	
			$scope.newRule.stay[0] = $scope.newRule.stay[0].slice(0,-1);
			return false;
		}
		else if(isNaN(maxLength) || $scope.newRule.stay[1].includes('.') )
		{	
			$scope.newRule.stay[1] = $scope.newRule.stay[1].slice(0,-1);
			return false;
		}

		// edit length of stay 
		$scope.displayedDates[number].lengthOfStay = $scope.newRule.stay[0] + '-' + $scope.newRule.stay[1];
	}

	// booking rules
	$scope.bookingRulesVisible = true;
    $scope.earlyBird = false;
    $scope.lastMinute = false;
    $scope.dateBound = false;

    $scope.calendar = {};

    	$scope.bookingRules = function(index)
	{	
		for(var i = 2; i < 5; i++){
			$('.centerButtons').eq(i).find('button').removeClass('clickedRooms');
		}
		$('.centerButtons').eq(index+2).find('button').addClass('clickedRooms');
		
		$scope.earlyBird = $scope.lastMinute = $scope.dateBound = false;

		$scope.calendar.now = $scope.calendar.to = new Date();

		$scope.calendar.days = 0;

		switch(index){
			case 0:
			$scope.lastMinute = true;
			break;
			case 1:
			$scope.earlyBird = true;
			break;
			case 2:
			$scope.dateBound = true;
			$scope.calendar.bookNow = $scope.calendar.bookTo = new Date();
			break;
		}

	}

	$scope.addBookingRule = function(name)
	{	
		if($scope.calendar.now > $scope.calendar.to){return alert('please type your date properly')};

		$scope.ratePlanForm.bookingRule = {};
		$scope.ratePlanForm.bookingRule.name = name;

		if(name == "Date Bound")
		{	
			if($scope.calendar.bookNow > $scope.calendar.bookTo){return alert('please type your date properly')};
			
			$scope.ratePlanForm.bookingRule.stayDateFrom = $scope.calendar.now;
			$scope.ratePlanForm.bookingRule.stayDateTo =  $scope.calendar.to;
			$scope.ratePlanForm.bookingRule.bookDateFrom = $scope.calendar.bookNow;
			$scope.ratePlanForm.bookingRule.bookDateTo = $scope.calendar.bookTo;
		}
		else
		{	
			$scope.ratePlanForm.bookingRule.days = $scope.calendar.days;
			$scope.ratePlanForm.bookingRule.activeFrom = $scope.calendar.now;
			$scope.ratePlanForm.bookingRule.activeTo =   $scope.calendar.to;
		}

		$scope.bookingRulesVisible = false;
		$scope.earlyBird = $scope.lastMinute = $scope.dateBound = false;
		
	}

	$scope.cancelBookingRule = function()
	{
		$scope.earlyBird = $scope.lastMinute = $scope.dateBound = false;
		for(var i = 2; i < 5; i++){
			$('.centerButtons').eq(i).find('button').removeClass('clickedRooms');
		}
	}

	$scope.deleteBookingRule = function()
	{
		delete $scope.ratePlanForm.bookingRule;
		
		for(var i = 2; i < 5; i++)
		{
			$('.centerButtons').eq(i).find('button').removeClass('clickedRooms');
		}

		$scope.bookingRulesVisible = true;
	}

	$scope.editBookingRule = function(name)
	{	
		
		$scope.bookingRulesVisible = true;

		switch(name){
			case 'Date Bound':
			$scope.dateBound = true;
			break;
			case 'Early Bird':
			$scope.earlyBird = true;
			break;
			case 'Last Minute':
			$scope.lastMinute = true;
			break;
		}
	}



}]);